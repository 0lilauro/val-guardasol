<!DOCTYPE html>
<html lang="pt-BR">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Val Guarda-Sol te ajuda a encontrar o melhor lugar para seu dia de sol. ">
    <meta name="author" content="Val Guarda-Sol">
    <link rel="icon" href="img/icon.ico">

    <title>Val Guarda-Sol | Meus guarda-sóis</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">José Barbosa</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="anunciar.php" name="alugar">Anunciar guarda sol</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="meus-guarda-sois.php" name="alugar">Meus guarda-sóis</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="home-dono.php" id="historico" name="historico">Histórico de aluguel</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">Perfil</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="logout.php" id="logout" name="logout">Logout</a>
        </li>
      </ul>
    </nav>
    <header class="content-section d-flex" style="background: url(img/praia.jpg);">
      <div class="container text-center my-auto">
        <h2 class="mb-5" style="color: #fff; text-shadow: 2px 2px 1px rgba(0,0,0,.3)">Guarda-sóis já cadastrados</h2>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- About -->
    <section class="content-section bg-light" id="sobre">
      <div class="container text-center my-auto">
        <div class="row">


          <div class="container text-left my-auto">
            <h4 class="mb-3" style="color: #444;">Buscar por:</h4>
          </div>
          <form id="form-search" class="row col-md-12" name="form-search">
            <div class="input-group mb-3 col-md-3">
              <select id="inputState" class="form-control search-input-btn">
                <option selected>ID do guarda-sol</option>
                <option>Descrição do guarda-sol</option>
                <option>Tamanho</option>
                <option>Preço</option>
              </select>
            </div>
            <div class="input-group mb-3 col-md-9">
              <input type="text" class="form-control" placeholder="Digite a descrição do que procura" aria-label="Digite a descrição do que procura" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary search-input-btn" type="button"><i class="fas fa-search"></i></button>
              </div>

            </div>
          </form>
          
        </div>
        
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" id="aluguel_id">ID do guarda-sol</th>
              <th scope="col" id="aluguel_id">Foto</th>
              <th scope="col" id="aluguel_desc">Descrição do guarda-sol</th>
              <th scope="col" id="aluguel_edit">Tamanho</th>
              <th scope="col" id="aluguel_preco">Preço</th>
              <th scope="col" id="aluguel_edit">Editar registro</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            
          </tbody>
        </table>
        
      </div>
</section>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
