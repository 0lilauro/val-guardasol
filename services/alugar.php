
<?php 
    session_start();
    require_once '../config/const.php';
    require_once("../config/config.php");
    require_once("../models/guardasol.php");

    try {
        if ($_POST && isset($_POST) && $_SESSION && isset($_SESSION)) {
            if(isset($_POST['id']) && !empty($_POST['id']) && isset($_SESSION['usuario']['id']) && !empty($_SESSION['usuario']['id'])){
                $guardasol = new GuardaSol();
                $result = $guardasol->alugarGuardaSol($_SESSION['usuario']['id'], $_POST['id']);
                $response = array(
                    "status"=> $result,
                    "data" => "All right",
                    "name" => "usuario"
                );
                echo json_encode($response);
            }
            else  {
                $response = array(
                    "status"=> false,
                    "data" => "POST data is empty1"
                );
                echo json_encode($response);
            }
        }
        else {
            $response = array(
                "status"=> false,
                "data" => "POST data is empty2"
            );
            echo json_encode($response);
        }
    }
    catch(Exception $e){
        $response = array(
            "status"=> false,
            "data" => $e
        );
        echo json_encode($response);
    }