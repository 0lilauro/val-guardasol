class ServicesRequest{
    constructor(){
        // this._request = "http://localhost";
        // this._request_services = "http://localhost"
        this._request = "http://localhost:1200";
        this._request_services = "http://localhost:1200/services";

    }
    //Getter
    get request(){
        return this._request;
    }
    get requestServices(){
        return this._request_services;
    }
    //Setter
    set request(value){
        this._request = value;
    }
    set requestServices(value){
        this._request_services = value;
    }

    requestLogin(email,senha,mode=0,){
        if (
            this.validateEmail(email) &&
            String(senha).trim() && senha.length > 0
           ){
            let url = this.requestServices +"/login.php"
            let data = {
                email: email,
                senha: senha,
                mode: mode
            }
            let form_data = new FormData();
            form_data.append("email",email);
            form_data.append("senha",senha);
            form_data.append("mode",mode);

            const options = {
                method: 'POST',
                headers: new Headers(),
                body: form_data
            };
            let result;
            
            (async () => {
                const rawResponse = await fetch(url,options);
                result = await rawResponse.json();
                console.log(result);            
                if(result){
                    if(result["status"]) {
                        window.location.href = this.request+`/home-${result['name']}.php`;
                    }
                    else {
                        msg = "Este email ou a senha estao incorretos, por favor tente novamente..."
                        this.errorLogin(msg);
                    }
                }
                else {
                    msg = "Algo de errado houve ao fazer o seu login, por favor tente novamente...."
                    this.errorLogin(msg);
                }
            })();
        }
        else {
            return false;
        }
    }
    errorLogin(
        msg = ""
    ){
        
    }
    clearErrorLogin(){

    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    alugarGuardaSol(id){
        let url = this.requestServices +"/alugar.php"
        
        let form_data = new FormData();
        form_data.append("id",id);
        const options = {
            method: 'POST',
            headers: new Headers(),
            body: form_data
        };
        let result;
        (async () => {
            const rawResponse = await fetch(url,options);
            result = await rawResponse.json();
            console.log(result);            
            if(result){
                if(result["status"]) {
                    window.location.href = this.request+`/home-${result['name']}.php`;
                }
                else {
                   alert('Erro ao fazer o aluguel');
                }
            }
            else {
                alert('Erro ao fazer o aluguel');
            }
        })();
    }

    deleteAluguel(id){
        let url = this.requestServices +"/deleteAluguel.php"
        
        let form_data = new FormData();
        form_data.append("id",id);
        const options = {
            method: 'POST',
            headers: new Headers(),
            body: form_data
        };
        let result;
        (async () => {
            const rawResponse = await fetch(url,options);
            result = await rawResponse.json();
            console.log(result);            
            if(result){
                if(result["status"]) {
                    window.location.href = this.request+`/home-${result['name']}.php`;
                }
                else {
                   alert('Erro ao deletar o aluguel');
                }
            }
            else {
                alert('Erro ao deletar o aluguel');
            }
        })();
    }

}