<?php
session_start();

class Dono {
    public $id;
    public $nome;
    public $email;
    public $senha;
    public $cpf;
    public $telefone;
    public $nota;

    private $con;

    function __construct(
    $id=null,
    $nome=null,
    $email=null,
    $senha=null,
    $cpf=null,
    $telefone=null,
    $nota=null

   ) {
        $this->id = $id;
        $this->nome = $nome;
        $this->email = $email;
        $this->senha = $senha;
        $this->cpf = $cpf;
        $this->telefone = $telefone;
        $this->nota = $nota;
        $con = new Conect();
        $this->con = $con->Connectar();
    }

    public function Registrar() {
        try {
            if(!$this->emailExiste($this->email)) {
                $sth = $this->con->prepare("INSERT INTO dono VALUES (null, :NOME, :EMAIL,:SENHA, :CPF, :TELEFONE, DEFAULT) ");
                $sth->bindValue(':NOME',$this->nome);
                $sth->bindValue(':EMAIL',$this->email);
                $sth->bindValue(':SENHA',md5($this->senha));
                $sth->bindValue(':CPF',$this->cpf);
                $sth->bindValue(':TELEFONE',$this->telefone);
                $result = $sth->execute();
            }
            else  {
                $result = false;
            }
            return $result;
        } catch (Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
                <div class='alert alert-danger'>
                <strong>Erro!</strong>
                    <pre>
                    ".$e."
                    /pre>
                </div>";
            return false;
        }
    }
    
    
    public function Login() {
        try {
            $sth = $this->con->prepare("SELECT D.id as 'id' ,D.nome as 'nome', D.email as 'email', D.telefone as 'telefone', D.cpf as 'cpf', D.nota as 'nota' FROM dono as D WHERE D.email = :EMAIL AND D.senha = :SENHA");
            $sth->bindValue(':EMAIL',$this->email);
            $sth->bindValue(':SENHA',md5($this->senha));
            $sth->execute();
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            if (count($result) != 1) {
                return false;
            }
            else {
                $dono = $result[0];
                $_SESSION['dono'] = $dono;
                return true;
            }
        }
        catch(Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }
    
    public function emailExiste($email) {
        try {
            $sth = $this->con->prepare("SELECT count(*) as 'counter' FROM dono as D WHERE D.email = :EMAIL");
            $sth->bindValue(':EMAIL',$email);
            $sth->execute();
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            if($result) {
                if ($result[0]['counter'] < 1){
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
            
        }
        catch(Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }

    public function Logout() {
        session_destroy();
    }
}
?>