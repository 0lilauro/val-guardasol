<!DOCTYPE html>
<html lang="pt-BR">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Val Guarda-Sol te ajuda a encontrar o melhor lugar para seu dia de sol. ">
    <meta name="author" content="Val Guarda-Sol">
    <link rel="icon" href="img/icon.ico">

    <title>Val Guarda-Sol | Início</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger" href="#page-top">Val Guarda-Sol</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#page-top">Início</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#sobre">Sobre</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#comofunciona">Como Funciona</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="login.php">Login</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#cadastro">Cadastre-se</a>
        </li>
      </ul>
    </nav>

    <!-- Header -->
    <header class="masthead d-flex" style="background: url(img/background1.jpg);">
      <div class="container text-center my-auto">
        <h1 class="mb-1" style="color: white;">Val Guarda-Sol</h1>
        <h3 class="mb-5">
          <em style="color: white;">A melhor opção para sua praia.</em>
        </h3>
        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#sobre">Descubra mais</a>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- About -->
    <section class="content-section bg-light" id="sobre">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>Val Guarda-Sol te ajuda a encontrar o melhor lugar para seu dia de sol. </h2>
            <p class="lead mb-5">Ao utilizar nossos serviços, você tem acesso a informações de diversos guarda-sóis, como preço, localização e poderá reservar aquele que mais lhe agrada.</p>
            <a class="btn btn-dark btn-xl js-scroll-trigger" href="#comofunciona">Veja como funciona</a>
          </div>
        </div>
      </div>
    </section>

    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="comofunciona">
      <div class="container">
        <div class="content-section-heading">
          <h3 class="text-secondary mb-0">O serviço</h3>
          <h2 class="mb-5">Como Funciona</h2>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="icon-umbrella"></i>
            </span>
            <h4>
              <strong>Cadastro de Guarda-Sóis</strong>
            </h4>
            <p class="text-faded mb-0">Locais com guarda-sóis alugáveis cadastram-se no site.</p>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="icon-list"></i>
            </span>
            <h4>
              <strong>Apresentação</strong>
            </h4>
            <p class="text-faded mb-0">Os guarda-sóis cadastrados são exibidos para os usuários.</p>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="icon-wallet"></i>
            </span>
            <h4>
              <strong>Contratação</strong>
            </h4>
            <p class="text-faded mb-0">Os usuários contratam o serviço de guarda-sol e o utilizam.</p>
          </div>
          <div class="col-lg-3 col-md-6">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <i class="icon-like"></i>
            </span>
            <h4>
              <strong>Após a experiência</strong>
            </h4>
            <p class="text-faded mb-0">O usuário e o dono do guarda-sol trocam avaliações que são compartilhadas na rede.</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Callout -->
    <section class="callout" id="login">
      <div class="container text-center">
        <h2 class="mx-auto mb-5" style="color:white;">Já possui uma conta? Clique no botão para realizar o login:</h2>
        <a class="btn btn-primary btn-xl" href="login.php">Entrar</a>
      </div>
    </section>

    <!-- Call to Action -->
    <section class="content-section bg-primary text-white" id="cadastro">
      <div class="container text-center">
        <h2 class="mb-4">Ainda não possui uma conta? Cadastre-se agora!</h2>
        <a href="cadastro-usuario.php" class="btn btn-xl btn-light mr-4">Quero alugar um guarda-sol!</a>
        <a href="cadastro-dono.php" class="btn btn-xl btn-dark">Quero ofertar meu guarda-sol!</a>
      </div>
    </section>

    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="https://www.facebook.com">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="https://www.instagram.com">
              <i class="icon-social-instagram"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0"><i>Copyright  &copy; Val Guarda-Sol 2018</i></p>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
