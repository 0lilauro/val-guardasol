<?php
session_start();
header("Content-Type: text/html; charset=utf8");

define('BD_SERVER', 'mysql:host=localhost;dbname=val_db1;charset=utf8');
define('BD_USER', 'root');
define('BD_PASSWORD', '');

try {

	$con = new PDO(BD_SERVER, BD_USER, BD_PASSWORD);
	
} 
catch (PDOException $ex) {
	echo 'Erro'.$ex->getMessage();
}
?>