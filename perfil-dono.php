<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/icon.ico">


    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">

    <title>Val Guarda-Sol | Perfil</title>
  </head>
  <body id="page-top">
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">José Barbosa</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="anunciar.php" name="alugar">Anunciar guarda sol</a>
        </li>
        <li class="sidebar-nav-item">63
          
          <a class="js-scroll-trigger" href="meus-guarda-sois.php" name="alugar">Meus guarda-sóis</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="home-dono.php" id="historico" name="historico">Histórico de aluguel</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">Perfil</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="logout.php" id="logout" name="logout">Logout</a>
        </li>
      </ul>
    </nav>
    <div class="my-auto container" style="padding: 10%">
      <div class="row">
        <div class="col-md-3"></div>

          <div class="col-md-6 mx-auto">
            
            <h2 class="text-center titulo">Seu perfil</h2>
            <form method="post" action="#">
              <div class="form-group row">
                <label for="nome" class="col-form-label">Nome Físico ou Jurídico:</label>
                <input type="text" name="nome" id="nome" class="form-control" placeholder="Digite seu nome completo..." autofocus required><br><br>
                <label for="email" class="col-form-label">Endereço de Email:</label>
                <input type="text" name="email" id="email" class="form-control" placeholder="Digite seu email..." required><br><br>
                <label for="senha" class="col-form-label">Senha:</label>
                <input type="password" name="senha" id="senha" class="form-control" placeholder="Digite sua senha..." required><br><br>
                <label for="cpf" class="col-form-label">CPF do proprietário:</label>
                <input type="text" name="cpf" id="cpf" class="form-control" placeholder="Digite seu CPF..." maxlength="14" required><br><br>
                <label for="telefone" class="col-form-label">Telefone (com DDD):</label>
                <input type="text" name="telefone" id="telefone" class="form-control" placeholder="Digite seu telefone..." maxlength="11" required><br><br><br>
                
                <button type="submit" class="btn btn-dark btn-lg btn-block mt-3">Atualizar perfil</button>
              </div>              
            </form>
            <center>
            <p class="text-muted small mb-0"><i>Copyright  &copy; Val Guarda-Sol 2018</i></p>
            </center>

         </div>
       <div class="col-md-3"></div>
      </div>      
    </div>
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>
  </body>
</html>