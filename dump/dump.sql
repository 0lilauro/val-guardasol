-- --------------------------------------------
-- --------------------------------------------
-- ----------- MYSQL DATABASE -----------------
-- --------------------------------------------
-- --------------------------------------------
-- Version: MYSQL 5.7.4 -----------------------
-- Author: Caio -------------------------------
-- --------------------------------------------

-- DATABASE:

  SET GLOBAL max_allowed_packet=1073741824;
  -- SET NAMES UTF8;
	DROP DATABASE IF EXISTS val_db;
	CREATE DATABASE val_db;
  USE val_db;



-- TABLE:

CREATE TABLE dono (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(64) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `telefone` char(11) NOT NULL,
  `nota` double(4,2) NOT NULL
)ENGINE=INNODB;

CREATE TABLE guarda_sol (
  `id` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `tamanho` varchar(40) NOT NULL,
  `preco` double(9,2) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `nota` double(4,2) NOT NULL,

  `id_dono` int(11) NOT NULL
)ENGINE=INNODB;

CREATE TABLE usuario (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(64) NOT NULL,
  `cpf` char(11) NOT NULL,
  `telefone` char(11) NOT NULL,
  `nota` double(4,2) NOT NULL DEFAULT '10',
  `nivel` int(11) NOT NULL DEFAULT '0'
  ) ENGINE=INNODB;

CREATE TABLE `aluguel` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `data_aluguel` DATETIME DEFAULT CURRENT_TIMESTAMP,

  `id_usuario` int(11) NOT NULL,
  `id_guarda_sol` int(11) NOT NULL
) ENGINE=INNODB;
	
-- PRIMARY KEY:

ALTER TABLE dono
	ADD CONSTRAINT PK_DONO
	PRIMARY KEY (id);
    ALTER TABLE dono MODIFY COLUMN id INT NOT NULL AUTO_INCREMENT;

ALTER TABLE guarda_sol
	ADD CONSTRAINT PK_GUARDA_SOL
	PRIMARY KEY (id);
    ALTER TABLE guarda_sol MODIFY COLUMN id INT NOT NULL AUTO_INCREMENT;

ALTER TABLE usuario
	ADD CONSTRAINT PK_USUARIO
	PRIMARY KEY (id);
    ALTER TABLE usuario MODIFY COLUMN id INT NOT NULL AUTO_INCREMENT;

ALTER TABLE aluguel
	ADD CONSTRAINT PK_ALUGUEL
	PRIMARY KEY (id);
    ALTER TABLE aluguel MODIFY COLUMN id INT NOT NULL AUTO_INCREMENT;

-- FOREIGN KEY:

ALTER TABLE guarda_sol 
	ADD CONSTRAINT FK_GUARDASOL_IDDONO
	FOREIGN KEY(id_dono)
REFERENCES dono(id);

ALTER TABLE aluguel 
	ADD CONSTRAINT FK_ALUGUEL_IDUSUARIO
	FOREIGN KEY(id_usuario)
REFERENCES usuario(id);

ALTER TABLE aluguel 
	ADD CONSTRAINT FK_ALUGUEL_IDGUARDASOL
	FOREIGN KEY(id_guarda_sol)
REFERENCES guarda_sol(id);



-- INSERT

INSERT INTO `usuario` VALUES
(null, 'Jorge', 'jorge@email.com', md5('123'), '12312332132', '87654321', DEFAULT, DEFAULT);
