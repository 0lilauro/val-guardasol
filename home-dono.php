<?php
  session_start();
  require_once './config/const.php';
  require_once './config/config.php';
  require_once './models/guardasol.php';


  if(!$_SESSION['dono']){
    header("location:".DEFAULT_URL."/");
  }
  
  if($_SESSION['usuario']) {
    header("location:".DEFAULT_URL."/home-usuario.php");
  }

  $dono = $_SESSION['dono'];
  $guardasol = new GuardaSol();
  $gs = $guardasol->historicoDono($dono['id']);
?>
<!DOCTYPE html>
<html lang="pt-BR">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Val Guarda-Sol te ajuda a encontrar o melhor lugar para seu dia de sol. ">
    <meta name="author" content="Val Guarda-Sol">
    <link rel="icon" href="img/icon.ico">

    <title>Val Guarda-Sol | Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">
    <script src="./js/Controller/services.js"></script>
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">José Barbosa</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="anunciar.php" name="alugar">Anunciar guarda sol</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="meus-guarda-sois.php" name="alugar">Meus guarda-sóis</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="home-dono.php" id="historico" name="historico">Histórico de aluguel</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">Perfil</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="logout.php" id="logout" name="logout">Logout</a>
        </li>
      </ul>
    </nav>

    <!-- About -->
    <section class="content-section bg-light" id="sobre"><div class="container text-center my-auto">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" id="aluguel_id">ID do aluguel</th>
              <th scope="col" id="aluguel_data">Data de aluguel</th>
              <th scope="col" id="aluguel_desc">Descrição do guarda-sol</th>
              <th scope="col" id="aluguel_loc">Locatário</th>
              <th scope="col" id="aluguel_preco">Preço</th>
              <th scope="col" id="aluguel_edit">Editar registro</th>
            </tr>
          </thead>
          <tbody>
            <?php if($gs):?>
              <?php foreach($gs as $key => $line): ?>
                <tr>
                  <th scope="row"><?=$line['id_aluguel']?></th>
                  <td><?=$line['data']?></td>
                  <td><?=$line['descricao']?></td>
                  <td><?=$line['nome']?></td>
                  <td>R$ <?=str_replace(".",",",$line['preco'])?></td>
                  <td>
                    <a class="btn btn-dark btn-xl edit-register-hdono" id="D-<?=$line['id_aluguel']?>" type="submit" name="excluir">Excluir</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
            <tr>
              <td colspan="6">Nenhum guarda-sol alugado ainda...</td>
            </tr>
            <?php endif;?>
            <!-- <tr>
              <th scope="row">1</th>
              <td>05/07/2018</td>
              <td>Colorido e grande</td>
              <td>Mariana</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="excluir">Excluir</button>
              </td>
            </tr> -->

          </tbody>
        </table>
        
      </div>
      <div class="my-auto bottom-div">
        <center>
          <a class="btn btn-primary btn-xl" href="alugar.php" id="novo-aluguel">Anunciar guarda-sol</a>
        </center>
      </div>

    <script>
      let btns = document.querySelectorAll(".edit-register-hdono");
      console.log(btns);
      btns.forEach((btn,index)=>{
          let id = parseInt(btn.id.replace('D-',''))
          btn.addEventListener('click',function(e){
              e.preventDefault();
              service = new ServicesRequest();
              result = service.deleteAluguel(id)
          });
      });
    </script>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
