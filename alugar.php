<?php
  session_start();

  require_once './config/const.php';
  require_once './config/config.php';
  require_once './models/guardasol.php';

  if(!$_SESSION['usuario']){
    header("location:".DEFAULT_URL."/");
  }

  if($_SESSION['dono']) {
    header("location:".DEFAULT_URL."/home-dono.php");
  }
  
  $user = $_SESSION['usuario'];
  $guardasol = new GuardaSol();
  
  
  if($_GET){
    if(
      !empty($_GET['SearchValue']) && !empty($_GET['SearchRow']) &&
      isset($_GET['SearchValue']) && isset($_GET['SearchRow'])
    ){
      $gs = $guardasol->pesquisarGuardaSol($_GET['SearchRow'],$_GET['SearchValue']);
    }
    else {
      $gs = $guardasol->pesquisarGuardaSol();
    }
  }
  else {
    $gs = $guardasol->pesquisarGuardaSol();
  }
?>
<!DOCTYPE html>
<html lang="pt-BR">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Val Guarda-Sol te ajuda a encontrar o melhor lugar para seu dia de sol. ">
    <meta name="author" content="Val Guarda-Sol">
    <link rel="icon" href="img/icon.ico">

    <title>Val Guarda-Sol | Alugar</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">
    <script src="./js/Controller/services.js"></script>

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
<<<<<<< HEAD
          <a class="js-scroll-trigger perfil" href="perfil-user.php" name="perfil">Maraiana Barbosa</a>
=======
          <a class="js-scroll-trigger perfil" href="perfil.php" name="perfil"><?=$user['nome']?></a>
>>>>>>> origin/lauro
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="alugar.php" name="alugar">Alugar guarda-sol</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="home-usuario.php" id="historico" name="historico">Histórico de aluguel</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger perfil" href="perfil-user.php" name="perfil">Perfil</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="logout.php" id="logout" name="logout">Logout</a>
        </li>
      </ul>
    </nav>
    <header class="content-section d-flex" style="background: url(img/praia.jpg);">
      <div class="container text-center my-auto">
        <h2 class="mb-5" style="color: #fff; text-shadow: 2px 2px 1px rgba(0,0,0,.3)">Encontre aqui o seu guarda-sol ideal</h2>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- About -->
    <section class="content-section bg-light" id="sobre">
      <div class="container text-center my-auto">
        <div class="row">


          <div class="container text-left my-auto">
            <h4 class="mb-3" style="color: #444;">Buscar por:</h4>
          </div>
          <form id="form-search" class="row col-md-12" name="form-search">
            <div class="input-group mb-3 col-md-3">
              <select id="inputState" name="SearchRow" class="form-control search-input-btn">
                <option value="0" selected>ID do guarda-sol</option>
                <option value="1">Descrição do guarda-sol</option>
                <option value="2">Dono</option>
                <option value="3">Tamanho</option>
                <option value="4">Preço</option>
              </select>
            </div>
            <div class="input-group mb-3 col-md-9">
              <input type="text" class="form-control" name="SearchValue" placeholder="Digite a descrição do que procura" aria-label="Digite a descrição do que procura" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary search-input-btn" type="submit"><i class="fas fa-search"></i></button>
              </div>

            </div>
          </form>
          
        </div>
        
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" id="aluguel_id">ID do guarda-sol</th>
              <th scope="col" id="aluguel_desc">Descrição do guarda-sol</th>
              <th scope="col" id="aluguel_dono">Dono</th>
              <th scope="col" id="aluguel_edit">Tamanho</th>
              <th scope="col" id="aluguel_preco">Preço</th>
            </tr>
          </thead>
          <tbody>
            <?php if($gs):?>
              <?php foreach($gs as $key => $line): ?>
                <tr>
                  <th scope="row"><?=$line['id_guardasol']?></th>
                  <td><?=$line['descricao']?></td>
                  <td><?=$line['nome']?></td>
                  <td><?=$line['tamanho']?></td>
                  <td>R$ <?=str_replace(".",",",$line['preco'])?></td>
                  <td>
                    <button class="btn btn-dark btn-xl alugar-button" type="submit" id="AL-<?=$line['id_guardasol']?>" name="vizualizar">Alugar</button>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
            <tr>
              <td colspan="6">Nenhum guarda-sol para alugar...</td>
            </tr>
            <?php endif;?>
            <!-- <tr>
              <th scope="row">1</th>
              <td>Colorido e grande</td>
              <td>Daniel</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="vizualizar">Alugar</button>
              </td>
<<<<<<< HEAD
              <td>
                <button class="btn btn-danger btn-xl" type="submit" name="excluir">Indisponível</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Colorido e grande</td>
              <td>Daniel</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="vizualizar">Alugar</button>
              </td>
              <td>
                <button class="btn btn-success btn-xl" type="submit" name="vizualizar">Disponível</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Colorido e grande</td>
              <td>Daniel</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="vizualizar">Alugar</button>
              </td>
              <td>
                <button class="btn btn-danger btn-xl" type="submit" name="excluir">Indisponível</button>
              </td>
            </tr>
=======
            </tr> -->
>>>>>>> origin/lauro
          </tbody>
        </table>
        
      </div>
</section>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <script>
      let btns = document.querySelectorAll(".alugar-button");
      console.log(btns);
      btns.forEach((btn,index)=>{
          let id = parseInt(btn.id.replace('AL-',''))
          btn.addEventListener('click',function(){
              service = new ServicesRequest();
              result = service.alugarGuardaSol(id)
          });
      });

    </script>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
