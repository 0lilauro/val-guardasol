-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 23-Jan-2019 às 18:26
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `val_db1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `donos`
--

DROP TABLE IF EXISTS `donos`;
CREATE TABLE IF NOT EXISTS `donos` (
  `idDono` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Senha` varchar(20) NOT NULL,
  `CPF/CNPJ` varchar(16) NOT NULL,
  `Telefone` char(11) NOT NULL,
  `Nota` double(2,2) NOT NULL,
  PRIMARY KEY (`idDono`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `guarda-sol`
--

DROP TABLE IF EXISTS `guarda-sol`;
CREATE TABLE IF NOT EXISTS `guarda-sol` (
  `idGuarda-Sol` int(11) NOT NULL AUTO_INCREMENT,
  `Donos_idDono` int(11) NOT NULL,
  `Descricao` varchar(100) NOT NULL,
  `Latitude` varchar(100) NOT NULL,
  `Longitude` varchar(100) NOT NULL,
  `Tamanho` varchar(40) NOT NULL,
  `Preço` double(9,2) NOT NULL,
  `Quantidade` int(11) NOT NULL,
  `Nota` double(2,2) NOT NULL,
  `Foto` longblob NOT NULL,
  PRIMARY KEY (`idGuarda-Sol`,`Donos_idDono`),
  KEY `fk_Guarda-Sol_Donos_idx` (`Donos_idDono`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuários` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Senha` varchar(20) NOT NULL,
  `CPF` char(11) NOT NULL,
  `Telefone` char(11) NOT NULL,
  `Nota` int(11) NOT NULL DEFAULT '10',
  `nivel` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idUsuários`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`idUsuários`, `Nome`, `Email`, `Senha`, `CPF`, `Telefone`, `Nota`, `nivel`) VALUES
(1, 'Jorge', 'jorge@email.com', '123', '123456', '87654321', 10, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios_aluga_guarda-sol`
--

DROP TABLE IF EXISTS `usuarios_aluga_guarda-sol`;
CREATE TABLE IF NOT EXISTS `usuarios_aluga_guarda-sol` (
  `Usuários_idUsuários` int(11) NOT NULL,
  `Guarda-Sol_idGuarda-Sol` int(11) NOT NULL,
  `Guarda-Sol_Donos_idDono` int(11) NOT NULL,
  `idAluguel` int(11) NOT NULL,
  `Total` double(9,2) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Usuários_idUsuários`,`Guarda-Sol_idGuarda-Sol`,`Guarda-Sol_Donos_idDono`,`idAluguel`),
  KEY `fk_Usuários_has_Guarda-Sol_Guarda-Sol1_idx` (`Guarda-Sol_idGuarda-Sol`,`Guarda-Sol_Donos_idDono`),
  KEY `fk_Usuários_has_Guarda-Sol_Usuários1_idx` (`Usuários_idUsuários`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `guarda-sol`
--
ALTER TABLE `guarda-sol`
  ADD CONSTRAINT `fk_Guarda-Sol_Donos` FOREIGN KEY (`Donos_idDono`) REFERENCES `donos` (`idDono`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuarios_aluga_guarda-sol`
--
ALTER TABLE `usuarios_aluga_guarda-sol`
  ADD CONSTRAINT `fk_Usuários_has_Guarda-Sol_Guarda-Sol1` FOREIGN KEY (`Guarda-Sol_idGuarda-Sol`,`Guarda-Sol_Donos_idDono`) REFERENCES `guarda-sol` (`idGuarda-Sol`, `Donos_idDono`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Usuários_has_Guarda-Sol_Usuários1` FOREIGN KEY (`Usuários_idUsuários`) REFERENCES `usuarios` (`idUsuários`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
