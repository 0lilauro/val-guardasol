<?php
session_start();

class Usuario {
    public $id;
    public $nome;
    public $email;
    public $senha;
    public $cpf;
    public $telefone;
    public $nota;
    public $nivel;

    private $con;

    function __construct(
    $id=null,
    $nome=null,
    $email=null,
    $senha=null,
    $cpf=null,
    $telefone=null
   ) {
        $this->id = $id;
        $this->nome = $nome;
        $this->email = $email;
        $this->senha = $senha;
        $this->cpf = $cpf;
        $this->telefone = $telefone;
        $this->nota = $nota;
        $this->nivel = $nivel;
        $con = new Conect();
        $this->con = $con->Connectar();
    }

    public function Registrar() {
        try {
            if(!$this->emailExiste($this->email)) {
                $sth = $this->con->prepare("INSERT INTO usuario VALUES (null, :NOME, :EMAIL,:SENHA, :CPF, :TELEFONE, DEFAULT, DEFAULT) ");
                $sth->bindValue(':NOME',$this->nome);
                $sth->bindValue(':EMAIL',$this->email);
                $sth->bindValue(':SENHA',md5($this->senha));
                $sth->bindValue(':CPF',$this->cpf);
                $sth->bindValue(':TELEFONE',$this->telefone);
                $result = $sth->execute();
            }
            else  {
                $result = false;
            }
            return $result;
        } catch (Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
                <div class='alert alert-danger'>
                <strong>Erro!</strong>
                    <pre>
                    ".$e."
                    /pre>
                </div>";
            return false;
        }
    }
    
    
    public function Login() {
        try {
            $sth = $this->con->prepare("SELECT U.id as 'id' ,U.nome as 'nome', U.email as 'email', U.telefone as 'telefone', U.cpf as 'cpf', U.nivel as 'nivel', U.nota as 'nota' FROM usuario as U WHERE U.email = :EMAIL AND U.senha = :SENHA");
            $sth->bindValue(':EMAIL',$this->email);
            $sth->bindValue(':SENHA',md5($this->senha));
            $sth->execute();
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            if (count($result) != 1) {
                return false;
            }
            else {
                $user = $result[0];
                $_SESSION['usuario'] = $user;
                return true;
            }
        }
        catch(Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }
    
    public function emailExiste($email) {
        try {
            $sth = $this->con->prepare("SELECT count(*) as 'counter' FROM usuario as U WHERE U.email = :EMAIL");
            $sth->bindValue(':EMAIL',$email);
            $sth->execute();
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            if($result) {
                if ($result[0]['counter'] < 1){
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
            
        }
        catch(Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }

    public function Logout() {
        session_destroy();
    }
}
?>