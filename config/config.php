<?php
use PDO;

class Conect {
	
	private $DB_LOCAL;
	private $DB_USER;
	private $DB_SENHA;
	private $DB_BANCO;


	function __construct($database="val_db",$user="",$senha="") {
	    $this->DB_LOCAL = "localhost";
		$this->DB_USER = $user;
		$this->DB_SENHA = $senha;
		$this->DB_BANCO = $database;
		return $this->Connectar();
   }

	public function Connectar(){
		$con = new PDO("mysql:host=localhost;dbname=val_db;charset=utf8", "root", "");
		// $con->query("SET NAMES utf8;");
		$con->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		try {
			$con->beginTransaction();
			$con->commit();
		} catch (PDOException $e) {
			print("Falha na Transmissão: ". $e->getMessage(). "\n");
			$con->rollback();
			 echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
                <strong>Erro!</strong>
                <pre>
                    ".$e."
                </pre>
            </div>";
			die();
		}
		return $con;
	}

	public function Desconectar($con){
		$con = NULL;
	}

	public function Inserir($sql){
		try {
    		$con = $this->Connectar();
	    	$count = $con->exec($sql);
			$this->Desconectar($con);
			return $count;
		} 
    	catch (Exception $e) {
    		 echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
                <strong>Erro!</strong>
                <pre>
                    ".$e."
                </pre>
            </div>";
            die();
    	}

	}
	public function Buscar($sql) {
		try {
    		$con= $this->Connectar();
			$result = $con->query($sql);
			$result = $result->fetchAll(\PDO::FETCH_ASSOC);
			$this->Desconectar($con);
			return $result;
		} 
    	catch (Exception $e) {
    		 echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
                <strong>Erro!</strong>
                <pre>
                    ".$e."
                </pre>
            </div>";
            die();
    	}		
	}
	function __destruct(){
		$this->DB_LOCAL = null;
		$this->DB_USER = null;
		$this->DB_SENHA = null;
		$this->DB_BANCO = null;
	}
}