
<?php 
    session_start();
    require_once("../config/config.php");
    require_once("../models/usuario.php");
    require_once("../models/dono.php");

    try {
        if (isset($_POST)) {
            if(isset($_POST['email']) && isset($_POST['senha']) && isset($_POST['mode'])){
                $type = $_POST['mode'];
                $name = "usuario";
                switch ($type) {                  
                    case '1':
                        $obj = new Dono(null,null,$_POST['email'],$_POST['senha']); 
                        $name = "dono";
                        break;
                    case '0':
                        $obj = new Usuario(null,null,$_POST['email'],$_POST['senha']); 
                        $name = "usuario";
                        break;

                    default:
                        $obj = new Usuario(null,null,$_POST['email'],$_POST['senha']); 
                        $name = "usuario";
                        break;
                }
                $result = $obj->Login(); 
                $response = array(
                    "status"=> $result,
                    "type"=> $type, 
                    "name"=> $name, 
                    "level" => 0
                );
                echo json_encode($response);
            }
            else  {
                $response = array(
                    "status"=> false,
                    "data" => "POST data is empty1"
                );
                echo json_encode($response);
            }
        }
        else {
            $response = array(
                "status"=> false,
                "data" => "POST data is empty2"
            );
            echo json_encode($response);
        }
    }
    catch(Exception $e){
        $response = array(
            "status"=> false,
            "data" => $e
        );
        echo json_encode($response);
    }