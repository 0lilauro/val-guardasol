<?php
session_start();

class GuardaSol{

    public $id;
    public $descricao;
    public $latitude;
    public $longitude;
    public $tamanho;
    public $preco;
    public $quantidade;
    public $nota;
    public $id_dono;

    private $con;
    
    function __construct(
        $id=null,
        $descricao=null,
        $latitude=null,
        $longitude=null,
        $tamanho=null,
        $preco=null,
        $id_dono=null
    ){
        $this->id = $id;
        $this->descricao = $descricao;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->tamanho = $tamanho;
        $this->preco = $preco;
        $this->quantidade = $quantidade;
        $this->id_dono = $id_dono;

        $con = new Conect();
        $this->con = $con->Connectar();
    }
    public function adicionarGuardaSol($usuario_id){
        try {
            $sth = $this->con->prepare("INSERT INTO guarda_sol VALUES(null,:DESCRICAO,:LATITUDE,:LONGITUDE,:TAMANHO,:PRECO,:QUATIDADE,0,:ID_DONO); ");
            $sth->bindValue(':DESCRICAO',$this->descricao);
            $sth->bindValue(':LATITUDE',$this->latitude);
            $sth->bindValue(':LONGITUDE',$this->longitude);
            $sth->bindValue(':TAMANHO',$this->tamanho);
            $sth->bindValue(':PRECO',$this->preco);
            $sth->bindValue(':QUATIDADE',$this->quatidade);
            $sth->bindValue(':ID_DONO',$this->id_dono);
            $result = $sth->execute();
        }
        catch(Exception $e){
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }

    public function historicoUsuario($usuario_id){
        try {
            if($usuario_id > 0 && isset($usuario_id)) {
                $sth = $this->con->prepare("
                SELECT 
                  G.id AS 'id_guarda_sol',
                  G.descricao AS 'descricao',
                  G.preco AS 'preco',
                  D.id AS 'id_dono',
                  D.nome AS 'nome',
                  A.id AS 'id_aluguel',
                  DATE_FORMAT(A.data_aluguel ,'%d/%m/%Y') AS 'data'
                FROM guarda_sol AS G 
                JOIN aluguel as A 
                  on A.id_guarda_sol = G.id
                JOIN usuario as U 
                  on A.id_usuario = U.id
                JOIN dono as D 
                  on D.id = G.id_dono
                WHERE U.id=:ID;");
                $sth->bindValue(':ID',$usuario_id);
                $sth->execute();
                $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
                if($result){
                    return $result;
                }
                else {
                    return false;
                }
            }
            else  {
                return false;
            }
        }
        catch(Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }
    public function historicoDono($dono_id){
        try {
            if($dono_id > 0 && isset($dono_id)) {
                $sth = $this->con->prepare("
                SELECT 
                  G.id AS 'id_guarda_sol',
                  G.descricao AS 'descricao',
                  G.preco AS 'preco',
                  U.id AS 'id_usuario',
                  U.nome AS 'nome',
                  A.id AS 'id_aluguel',
                  DATE_FORMAT(A.data_aluguel ,'%d/%m/%Y') AS 'data'
                FROM guarda_sol AS G 
                JOIN aluguel as A 
                  on A.id_guarda_sol = G.id
                JOIN usuario as U 
                  on A.id_usuario = U.id
                JOIN dono as D 
                  on D.id = G.id_dono
                WHERE D.id=:ID;");
                $sth->bindValue(':ID',$dono_id);
                $sth->execute();
                $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
                if($result){
                    return $result;
                }
                else {
                    return false;
                }
            }
            else  {
                return false;
            }
        }
        catch(Exception $e) {
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }

    public function alugarGuardaSol($user_id, $guardasol_id){
        try {
            $sth = $this->con->prepare("INSERT INTO aluguel VALUES (null,'alugado',DEFAULT,:ID_USER,:ID_GUARDASOL)");
            $sth->bindValue(':ID_USER',$user_id);
            $sth->bindValue(':ID_GUARDASOL',$guardasol_id);
            $result = $sth->execute();
            return  $result;
        }
        catch(Exception $e){
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }

    public function pesquisarGuardaSol($row=10000, $value=null){
        try {
            $sql = "
            SELECT 
              G.id as 'id_guardasol',
              G.descricao as 'descricao',
              G.tamanho as 'tamanho',
              G.preco as 'preco',
              D.nome as 'nome',
              D.id as 'id_dono'
            FROM guarda_sol as G 
            JOIN dono as D
              ON D.id = G.id_dono
              ";

            //   id-guardasol - 0
            //   descricao-guardasol - 1
            //   preco-guardasol - 2
            //   tamanho-guardasol - 3
            //   nome-dono - 4

            $append="";
            switch($row){
                case 0:
                    $append= " where G.id = :VALUE";
                    break;
                case 1:
                    $append= " where G.descricao like '%:VALUE%'";
                    break;
                case 2:
                    $append= " where G.tamanho like '%:VALUE%'";
                    break;
                case 3:
                    $append= " where G.preco like '%:VALUE%'";
                    break;
                case 4:
                    $append= " where D.nome like '%:VALUE%'";
                    break;
            }  
            $sth = $this->con->prepare($sql.$append);
            if($row <10){
                $sth->bindValue(':VALUE',$value);
            }
            $sth->execute();
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            if($result){
                return $result;
            }
            else {
                return false;
            }
        }
        catch(Exception $e){
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }

    public function deleteAluguel($id){
        try {
            $sth = $this->con->prepare("DELETE FROM aluguel WHERE id = :ID");
            $sth->bindValue(':ID',$id);
            $result = $sth->execute();
            return $result;
        }
        catch(Exception $e){
            echo "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css'>
            <div class='alert alert-danger'>
            <strong>Erro!</strong>
                <pre>
                ".$e."
                /pre>
            </div>";
            return false;
        }
    }
    
}
