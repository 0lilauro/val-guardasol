<!DOCTYPE html>
<html lang="pt-BR">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Val Guarda-Sol te ajuda a encontrar o melhor lugar para seu dia de sol. ">
    <meta name="author" content="Val Guarda-Sol">
    <link rel="icon" href="img/icon.ico">

    <title>Val Guarda-Sol | Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#guarda-sois" name="guarda-sois">Guarda-sóis cadastrados</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#alugueis" name="alugueis">Aluguéis já efetuados</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger perfil" href="#prop" name="prop">Proprietários de guarda-sóis</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#users" name="users">Locatários de guarda-sóis</a>
        </li>
      </ul>
    </nav>

    <!-- About -->
    <section class="content-section bg-light" id="guarda-sois"><div class="container text-center my-auto">
      <h2 class="mb-5" style="color: #444;">Guarda-sóis cadastrados</h2>
      <div class="table-wrapper-scroll-y">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" id="aluguel_id">ID do guarda-sol</th>
              <th scope="col" id="aluguel_foto">Foto</th>
              <th scope="col" id="aluguel_prop">Proprietário</th>
              <th scope="col" id="aluguel_desc">Descrição do guarda-sol</th>
              <th scope="col" id="aluguel_edit">Tamanho</th>
              <th scope="col" id="aluguel_preco">Preço</th>
              <th scope="col" id="aluguel_edit">Editar registro</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Colorido e grande</td>
              <td>Diâmetro de 2m</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="editar">Editar</button>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            
          </tbody>
        </table>
        </div>
      </section>



    <section class="content-section bg-light" id="alugueis"><div class="container text-center my-auto">
      <h2 class="mb-5" style="color: #444;">Aluguéis já efetuados</h2>
      <div class="table-wrapper-scroll-y">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" id="aluguel_id">ID do guarda-sol</th>
              <th scope="col" id="aluguel_foto">Foto</th>
              <th scope="col" id="aluguel_prop">Proprietário</th>
              <th scope="col" id="aluguel_prop">Locatário</th>
              <th scope="col" id="aluguel_desc">Descrição do guarda-sol</th>
              <th scope="col" id="aluguel_preco">Preço</th>
              <th scope="col" id="aluguel_edit">Editar registro</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td><img src="img/guarda-sol-icon.png" class="medium-icon"></td>
              <td>Daniel</td>
              <td>Lucas</td>
              <td>Colorido e grande</td>
              <td>R$ 20,00</td>
              <td>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
          </tbody>
        </table>
        </div>
      </section>



    <section class="content-section bg-light" id="prop"><div class="container text-center my-auto">
      <h2 class="mb-5" style="color: #444;">Proprietários de guarda-sóis</h2>
      <div class="table-wrapper-scroll-y">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" id="aluguel_id">ID do proprietário</th>
              <th scope="col" id="aluguel_foto">Nome</th>
              <th scope="col" id="aluguel_prop">E-mail</th>
              <th scope="col" id="aluguel_desc">CPF</th>
              <th scope="col" id="aluguel_edit">Telefone</th>
              <th scope="col" id="aluguel_edit">Editar registro</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-dono.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
          </tbody>
        </table>
        </div>
      </section>



    <section class="content-section bg-light" id="users"><div class="container text-center my-auto">
      <h2 class="mb-5" style="color: #444;">Locatários de guarda-sóis</h2>
      <div class="table-wrapper-scroll-y">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" id="aluguel_id">ID do locatário</th>
              <th scope="col" id="aluguel_foto">Nome</th>
              <th scope="col" id="aluguel_prop">E-mail</th>
              <th scope="col" id="aluguel_desc">CPF</th>
              <th scope="col" id="aluguel_edit">Telefone</th>
              <th scope="col" id="aluguel_edit">Editar registro</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>José</td>
              <td>jose@gmail.com</td>
              <td>134.134.134-13</td>
              <td>(31)99999-9999</td>
              <td>
                <a class="btn btn-dark btn-xl" name="editar" href="perfil-user.php">Editar</a>
                <button class="btn btn-dark btn-xl" type="submit" name="apagar">Apagar</button>
              </td>
            </tr>
          </tbody>
        </table>
        </div>
      </section>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
