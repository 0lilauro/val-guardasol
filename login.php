<?php
  session_start();
  require_once './config/const.php';
  if($_SESSION['usuario']){
    header("location:".DEFAULT_URL."/home-usuario.php");
  }
  if($_SESSION['dono']) {
    header("location:".DEFAULT_URL."/home-dono.php");
  }
?>
<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/icon.ico">

    <title>Val Guarda-Sol | Login</title>


    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">
    <script src="./js/Controller/services.js"></script>
  </head>

  <body>
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger" href="#page-top">Val Guarda-Sol</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="index.php">Início</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="index.php#sobre">Sobre</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="index.php#comofunciona">Como Funciona</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="login.php">Login</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="index.php#cadastro">Cadastre-se</a>
        </li>
      </ul>
    </nav>

    <div class="my-auto container" style="padding: 10% 20%">
      <form id="form-login" class="form-signin">
          <div class="container my-auto">
            <h3 class="mb-3" style="color: #444;">Login</h3>
          </div>
          <label for="inputEmail" class="sr-only">Email</label>
          <input type="email" id="inputEmail" class="form-control mb-3" name="inputEmail" placeholder="Email" required autofocus>
          <label for="inputPassword" class="sr-only">Senha</label>
          <input type="password" id="inputPassword" class="form-control mb-3" name="inputSenha" placeholder="Senha" required>

          <div class="px-0 btn-group btn-group-toggle form col-md-12 radio-g2 mb-3 no-padding" data-toggle="buttons">
            <label class="btn btn-secondary active col-md-6 no-padding">
              <input type="radio" id="option1" name="inputRadio" id="radio-usuario" autocomplete="off" checked value="0">Usuário
            </label>
            <label class="btn btn-secondary col-md-6 no-padding">
              <input type="radio" id="option2" name="inputRadio" id="radio-dono" autocomplete="off" value="1">Dono
            </label>
          </div>

          <div class="row mb-3">
            <div class="col-md-6"><button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button></div>
            <div class="col-md-6">
            <a type="button "class="btn btn-lg btn-block btn-dark" href="cadastro-usuario.php">Cadastre-se</a></div>
          </div>
          <p class="text-muted small mb-0"><i>Copyright  &copy; Val Guarda-Sol 2018</i></p>
      </form>
    </div>
    <script src="./js/index.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>