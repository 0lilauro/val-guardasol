
<?php 
    session_start();
    require_once '../config/const.php';
    require_once("../config/config.php");
    require_once("../models/guardasol.php");

    try {
        if ($_POST && isset($_POST)) {
            if(isset($_POST['id']) && !empty($_POST['id'])){
                $guardasol = new GuardaSol();
                $result = $guardasol->deleteAluguel($_POST['id']);
                $response = array(
                    "status"=> $result,
                    "data" => "All right",
                    "name" => "usuario"
                );
                echo json_encode($response);
            }
            else  {
                $response = array(
                    "status"=> false,
                    "data" => "POST data is empty1"
                );
                echo json_encode($response);
            }
        }
        else {
            $response = array(
                "status"=> false,
                "data" => "POST data is empty2"
            );
            echo json_encode($response);
        }
    }
    catch(Exception $e){
        $response = array(
            "status"=> false,
            "data" => $e
        );
        echo json_encode($response);
    }