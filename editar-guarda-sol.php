<!DOCTYPE html>
<html lang="pt-BR">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Val Guarda-Sol te ajuda a encontrar o melhor lugar para seu dia de sol. ">
    <meta name="author" content="Val Guarda-Sol">
    <link rel="icon" href="img/icon.ico">

    <title>Val Guarda-Sol | Anunciar</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">José Barbosa</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="anunciar.php" name="alugar">Anunciar guarda sol</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="meus-guarda-sois.php" name="alugar">Meus guarda-sóis</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="home-dono.php" id="historico" name="historico">Histórico de aluguel</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger perfil" href="perfil-dono.php" name="perfil">Perfil</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="logout.php" id="logout" name="logout">Logout</a>
        </li>
      </ul>
    </nav>

    <!-- About -->
    <section class="content-section bg-light" id="sobre">
      <div class="container text-center my-auto">
        <div class="row">


          <div class="container text-left my-auto">
            <h4 class="mb-3" style="color: #444;">Edite dados do guarda-sol:</h4>
          </div>
          <form id="form-cadastrar" class="col-md-12" name="form-cadastrar">
            <div class="mb-3">
             <p class="col-md-12 text-left mb-0">Descrição do guarda-sol</p>
              <input type="text" class="form-control col-md-12" id="desc" name="desc">
            </div>
            <div class="mb-3">
             <p class="col-md-12 text-left mb-0">Preço</p>
              <input type="text" class="form-control col-md-12" id="preco" name="preco">
            </div>
            <div class="mb-3 row">
             <div class="col-md-3">
               <p class="col-md-12 text-left mb-0">Latitude</p>
              <input type="text" class="form-control" id="lat" name="lat">
             </div>
             
             <div class="col-md-3">
               <p class="col-md-12 text-left mb-0">Longitude</p>
              <input type="text" class="form-control" id="long" name="long">
             </div>
             
           
             <div class="col-md-3">
               <p class="col-md-12 text-left mb-0">Tamanho</p>
              <input type="text" class="form-control" id="tamanho" name="tamanho">
             </div>
             
             <div class="col-md-3">
               <p class="col-md-12 text-left mb-0">Quantidde</p>
              <input type="number" class="form-control" id="quant" name="quant">
             </div>
             
             </div>
             <button class="btn btn-dark btn-xl" type="submit" name="anunciar" id="anunciar">Atualizar</button>
          </form>
          
        </div>
        
      </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
